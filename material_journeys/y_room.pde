void drawRoom(){
    
  stroke(255);
  strokeWeight(5);
  
  ////floor
  line (-roomX/2,0,-roomY/2, -roomX/2,0,roomY/2);
  line (-roomX/2,0,roomY/2,roomX/2,0,roomY/2);
  line (roomX/2,0,roomY/2,roomX/2,0,-roomY/2);
  line (roomX/2,0,-roomY/2, -roomX/2,0,-roomY/2);
  
  ////celing
  line (-roomX/2,roomZ,-roomY/2, -roomX/2,roomZ,roomY/2);
  line (-roomX/2,roomZ,roomY/2, roomX/2,roomZ,roomY/2);
  line (roomX/2,roomZ,roomY/2, roomX/2,roomZ,-roomY/2);
  line(roomX/2,roomZ,-roomY/2, -roomX/2,roomZ,-roomY/2);
  
  ////walls
  line (-roomX/2,0,-roomY/2, -roomX/2,roomZ,-roomY/2);
  line (-roomX/2,0,roomY/2, -roomX/2,roomZ,roomY/2);
  line (roomX/2,0,roomY/2,roomX/2,roomZ,roomY/2); 
  line (roomX/2,0,-roomY/2, roomX/2,roomZ,-roomY/2);
  
  
  float MinX = -roomX/2;
  float MaxX = roomX/2;
  float MinY = -roomY/2;
  float MaxY = roomY/2;
  float MinZ = -roomZ/2;
  float MaxZ = roomZ/2;
  
  float wL = 8000;
  float wH = 4000;
  float wOffset = 500;
  float hOffset = 200;

  ////window
  line (MinX,roomZ-hOffset,   MaxY-wOffset,    MinX,roomZ-hOffset,     MaxY-wOffset-wL);
  line (MinX,roomZ-hOffset-wH,MaxY-wOffset,    MinX,roomZ-hOffset-wH,  MaxY-wOffset-wL);
  
  line (MinX,roomZ-hOffset,   MaxY-wOffset,    MinX,roomZ-hOffset-wH,  MaxY-wOffset); 
  line (MinX,roomZ-hOffset,   MaxY-wOffset-wL, MinX,roomZ-hOffset-wH,  MaxY-wOffset-wL);
  
    
  ////beams
  for (int i = 1; i<=2; i++){
    
    line (MinX+(roomBay*i)-(roomBeamW/2), 0+roomBeamH, MinY, MinX+(roomBay*i)-(roomBeamW/2), 0+roomBeamH, MaxY);
    line (MinX+(roomBay*i)+(roomBeamW/2), 0+roomBeamH, MinY, MinX+(roomBay*i)+(roomBeamW/2), 0+roomBeamH, MaxY);
    line (MinX+(roomBay*i)-(roomBeamW/2), 0, MinY, MinX+(roomBay*i)-(roomBeamW/2), 0, MaxY);
    line (MinX+(roomBay*i)+(roomBeamW/2), 0, MinY, MinX+(roomBay*i)+(roomBeamW/2), 0, MaxY);
    
    line (MinX+(roomBay*i)-(roomBeamW/2), 0+roomBeamH, MinY, MinX+(roomBay*i)-(roomBeamW/2), 0, MinY);
    line (MinX+(roomBay*i)+(roomBeamW/2), 0+roomBeamH, MinY, MinX+(roomBay*i)+(roomBeamW/2), 0, MinY);
    line (MinX+(roomBay*i)-(roomBeamW/2), 0+roomBeamH, MaxY, MinX+(roomBay*i)-(roomBeamW/2), 0, MaxY);
    line (MinX+(roomBay*i)+(roomBeamW/2), 0+roomBeamH, MaxY, MinX+(roomBay*i)+(roomBeamW/2), 0, MaxY);
    
    line (MinX+(roomBay*i)-(roomBeamW/2), 0+roomBeamH, MinY,MinX+(roomBay*i)+(roomBeamW/2), 0+roomBeamH, MinY);
    line (MinX+(roomBay*i)-(roomBeamW/2), 0+roomBeamH, MaxY, MinX+(roomBay*i)+(roomBeamW/2), 0+roomBeamH, MaxY);
  
  }
  
  
}
