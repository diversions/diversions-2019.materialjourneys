int miscTotal;
float miscCountLeft = -roomX/2;
float miscCountRight = roomX/2;
float miscMax = roomX/2;

void drawMisc(){
    stroke(0,0,255);
    strokeWeight(5);
    noFill();
    
    miscCountLeft+=5;
    if(miscCountLeft>0){
        miscCountLeft=0;
    }
    
    
    float LMinX = -roomX/2;
    float LMaxX = miscCountLeft;
    float LMinY = -roomY/2;
    float LMaxY = roomY/2;
    
    ////ceiling
    line (LMinX, 0+roomBeamH, LMinY, LMinX, 0+roomBeamH, LMaxY);
    line (LMinX, 0+roomBeamH, LMaxY, LMaxX, 0+roomBeamH, LMaxY);
    line (LMaxX, 0+roomBeamH, LMaxY, LMaxX, 0+roomBeamH, LMinY);
    line (LMaxX, 0+roomBeamH, LMinY, LMinX, 0+roomBeamH, LMinY);      
    ////floor
    line (LMinX, roomZ, LMinY, LMinX, roomZ, LMaxY);
    line (LMinX, roomZ, LMaxY, LMaxX, roomZ, LMaxY);
    line (LMaxX, roomZ, LMaxY, LMaxX, roomZ, LMinY);
    line (LMaxX, roomZ, LMinY, LMinX, roomZ, LMinY);
    //////walls
    line (LMinX,0+roomBeamH, LMinY, LMinX, roomZ, LMinY);
    line (LMinX,0+roomBeamH, LMaxY, LMinX, roomZ, LMaxY);
    line (LMaxX,0+roomBeamH, LMaxY, LMaxX, roomZ, LMaxY); 
    line (LMaxX,0+roomBeamH, LMinY, LMaxX, roomZ, LMinY);
    
    miscCountRight-=5;
    if(miscCountRight<0){
        miscCountRight=0;
    }
    
    float RMinX = miscCountRight;
    float RMaxX = roomX/2;
    float RMinY = -roomY/2;
    float RMaxY = roomY/2;
    ////ceiling
    line (RMinX, 0+roomBeamH, RMinY, RMinX, 0+roomBeamH, RMaxY);
    line (RMinX, 0+roomBeamH, RMaxY, RMaxX, 0+roomBeamH, RMaxY);
    line (RMaxX, 0+roomBeamH, RMaxY, RMaxX, 0+roomBeamH, RMinY);
    line (RMaxX, 0+roomBeamH, RMinY, RMinX, 0+roomBeamH, RMinY);      
    ////floor
    line (RMinX, roomZ, RMinY, RMinX, roomZ, RMaxY);
    line (RMinX, roomZ, RMaxY, RMaxX, roomZ, RMaxY);
    line (RMaxX, roomZ, RMaxY, RMaxX, roomZ, RMinY);
    line (RMaxX, roomZ, RMinY, RMinX, roomZ, RMinY);
    //////walls
    line (RMinX,0+roomBeamH, RMinY, RMinX, roomZ, RMinY);
    line (RMinX,0+roomBeamH, RMaxY, RMinX, roomZ, RMaxY);
    line (RMaxX,0+roomBeamH, RMaxY, RMaxX, roomZ, RMaxY); 
    line (RMaxX,0+roomBeamH, RMinY, RMaxX, roomZ, RMinY);    
}


void initializeMisc(){
}

void playMisc(){
  try {
    
   fd = context.getAssets().openFd("misc_en.wav");
   if (french==true){
      fd = context.getAssets().openFd("misc_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
  }
  catch (IllegalArgumentException e) {
    e.printStackTrace();
  }
  catch (IllegalStateException e) {
    e.printStackTrace();
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
  playMiscAudio=false;
  
}
