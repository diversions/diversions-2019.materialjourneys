int num = 20;
int range = 50;

float[] ax = new float[num];
float[] ay = new float[num]; 
float[] az = new float[num]; 


void drawFly(){
  // Shift all elements 1 place to the left
  for(int i = 1; i < num; i++) {
    ax[i-1] = ax[i];
    ay[i-1] = ay[i];
    az[i-1] = az[i];
  }

  // Put a new value at the end of the array
  ax[num-1] += random(-range, range);
  ay[num-1] += random(-range, range);
  az[num-1] += random(-range, range);

  // Constrain all points to the screen
  ax[num-1] = constrain(ax[num-1], -roomX/2, roomX/2);
  ay[num-1] = constrain(ay[num-1], -roomY/2, roomY/2); 
  az[num-1] = constrain(az[num-1], -roomZ/2, roomZ/2); 
  
  strokeWeight(1);
  // Draw a line connecting the points
  for(int i=1; i<num; i++) {    
    float val = float(i)/num * 204.0 + 51;
    stroke(val);
    line(ax[i-1], az[i-1], ay[i-1], ax[i], az[i], ay[i]);
  }
}

void initializeFly(){
  pushMatrix();
    for(int i = 0; i < num; i++) {
      ax[i] = 0;
      ay[i] = -10*roomY;
      az[i] = -500;
    }  
  popMatrix();
}
