hairObject haObj;
ArrayList<hairObject> haObjs;

int hairsTotal=0;
int hairDensity = 200;
int hairLength = 0;
int hairMaxLength = int(roomZ);

///////////////////////////////////////////////

class hairObject{
  
  int idX;
  int idY;
  
  float x;
  float y;
  float z;  
  
  PShape hair;

  hairObject(int posX, int posY, float posZ){
    
    idX = posX;
    idY = posY;
    
    x = float(posX) - (roomX/2);
    y = float(posY) - (roomY/2);
    z = posZ * -0.5;
    

  }
  
  void drawHair(){
      
   strokeWeight(1);
   stroke(255,0,0);
   noFill();
    
    float var0 = random(-hairDensity/2, hairDensity/2);
    float var1 = random(-hairDensity/2, hairDensity/2);
    
    boolean drawHair = false;
         
    if (idX<(roomBay-(roomBeamW))){
      drawHair=true;
    }
    if (idX>((roomBay*2)+(roomBeamW))){
      drawHair=true;
    }
    if((idX>(roomBay+(roomBeamW)))&&(idX<((roomBay*2)-(roomBeamW)))){
      drawHair=true;
    } 
    
    if (drawHair==true){
      bezier(x, z, y, x-var0, z, y-var1, x+var0, z+hairLength, y, x+var0, z+hairLength, y);
    }
    
  }
    
}
  
////////////////////////////////////////////////////////


void drawHairs(){
  
 hairLength+=5;
 if (hairLength> hairMaxLength){ 
   hairLength=hairMaxLength;
 }
 
 for (int i=0; i< hairsTotal; i++){
      haObjs.get(i).drawHair();
      ////float var0 = (noise(millis(), ha.x)) * (hairDensity/2);
      ////float var1 = (noise(millis(), ha.z)) * (hairDensity/2);
      ////println(var0, " + ", ha.x );
      ////println(var1);
      ////float var0 = (noise(millis() * 0.0005, sin(ha.x))-0.5)* (hairDensity);
      ////float var1 = (noise(millis() * 0.0007, sin(ha.z) * 0.01)-0.5)* (hairDensity);
      ////println(var0, " + ", var1 );
      //float var0 = random(-hairDensity/2, hairDensity/2);
      //float var1 = random(-hairDensity/2, hairDensity/2);
      
      ////bezier(ha.x, ha.y, ha.z, ha.x-var0, ha.y, ha.z-var1, ha.x+var0, ha.y+hairLength, ha.z, ha.x+var0, ha.y+hairLength, ha.z);
 
      //if ((ha.x<7000)||(ha.x>15000)){
      //  bezier(ha.x, ha.z, ha.y, ha.x-var0, ha.z, ha.y-var1, ha.x+var0, ha.z+hairLength, ha.y, ha.x+var0, ha.z+hairLength, ha.y);
      //}
      //if((ha.x>7500)&&(ha.x<14500)){
      //  bezier(ha.x, ha.z, ha.y, ha.x-var0, ha.z, ha.y-var1, ha.x+var0, ha.z+hairLength, ha.y, ha.x+var0, ha.z+hairLength, ha.y);
      //}
 }
 
}
 
void initializeHairs(){
  
    for (int i = 0; i< roomX; i = i+hairDensity){
      for (int j = 0; j< roomY; j = j+hairDensity){
        haObj = new hairObject(i, j, roomZ);
        haObjs.add(haObj);
        hairsTotal++;
      }
    }
   
}

void playHair(){
  try {

    fd = context.getAssets().openFd("hair_en.wav");
    if (french==true){
      fd = context.getAssets().openFd("hair_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
    }
    
    catch (IllegalArgumentException e) {
    e.printStackTrace();
    }
    catch (IllegalStateException e) {
    e.printStackTrace();
    } 
    catch (IOException e) {
    e.printStackTrace();
    }
    playHairAudio=false;
}
   
