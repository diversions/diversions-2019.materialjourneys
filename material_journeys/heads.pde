float headsRotate = 0;
int headsTotal;
StringList headsList;

headObject hObj;
ArrayList<headObject> hObjs;

int headAge = 0;
int headAqr = 0;

///////////////////////////////////////////////

class headObject{
  
  int id;
  
  float l;
  float w;
  float h; 
  
  float x;
  float y;
  float z;  
  
  float r;
  
  String data;
  
  int age = int(random(0,2000));
  int aqr = int(random(age,2000));

  headObject(int identity, String dataString){
    
    id = identity;
    
    data = dataString;
    String dataBits[] = split(data, ",");
    
    l = float(dataBits[0]) * 3;
    w = float(dataBits[1]) * 3;
    h = float(dataBits[2]) * 3;
    
    x = random(-roomX, roomX); 
    y = random(-300, +300);
    z = random(-roomZ, roomZ);
    
    r = 0;

  }
  
  void drawHead(){
    
    sphereDetail(12);
    strokeWeight(1);
    stroke(255,255,255);
    fill(100);
     
    if (aqr<headAqr){
      noFill();
    }
    
    if (age<headAge){
      
      pushMatrix();
        translate(x, y, z);
          r = r+(PI/60);
          rotateY(r);
        sphere(l);
      popMatrix(); 
      
    }
  }
    
}
  
////////////////////////////////////////////////////////

void drawHeads(){
 
 for (int i=0; i< headsTotal; i++){
   hObjs.get(i).drawHead();
    //pushMatrix();
    //  headObject h = hObjs.get(i);
    //  translate(h.x, h.y, h.z);
    //    h.r = h.r+(PI/60);
    //    rotateY(h.r);
    //  sphere(h.l);
    //popMatrix();
 }
 
 headAge++;
 headAqr++;
 
}
 
void initializeHeads(){
  
  for (int i=headsTotal-1; i>=0; i--){
    String data = headsList.get(i);
    hObj = new headObject(i, data);
    hObjs.add(hObj);
  }
   
}

void playHeads(){
  try {
    
   fd = context.getAssets().openFd("heads_en.wav");
   if (french==true){
      fd = context.getAssets().openFd("heads_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
  }
  catch (IllegalArgumentException e) {
    e.printStackTrace();
  }
  catch (IllegalStateException e) {
    e.printStackTrace();
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
  playHeadsAudio=false;
  
}
