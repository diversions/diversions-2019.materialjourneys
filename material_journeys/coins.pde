int coinsTotal = 500;
float activeCoins = 1;

coinObject cObj;
ArrayList<coinObject> cObjs;

class coinObject {
  
  int id;
  int time = int(random(0,5000));
  float r=1;
  float x = random(-20,20);
  float y = random(-20,20);

  coinObject(int identity){
    
    id = identity;
    
  }
  
  void drawCoin(){
  
    if (time<activeCoins){
      stroke(0, 255, 0, 255-(r/50));
      strokeWeight(r/500);
      noFill();
      r=r+10;
      ellipse(x,y,r,r);
    }
  }
  
}


void drawCoins(){
  
  for (int i=coinsTotal-1; i>=0; i--){
    coinObject c = cObjs.get(i);
    c.drawCoin();
  }  
  
  activeCoins = activeCoins+0.1;
  
}

void initializeCoins(){
  
  for (int i=coinsTotal-1; i>=0; i--){
    cObj = new coinObject(i);
    cObjs.add(cObj);
  }
   
}

void playCoins(){
  try {

    fd = context.getAssets().openFd("coins_en.wav");
    if (french==true){
      fd = context.getAssets().openFd("coins_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
    }
    
    catch (IllegalArgumentException e) {
    e.printStackTrace();
    }
    catch (IllegalStateException e) {
    e.printStackTrace();
    } 
    catch (IOException e) {
    e.printStackTrace();
    }
    playCoinsAudio=false;
}
