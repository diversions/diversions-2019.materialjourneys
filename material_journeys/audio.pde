//===========================================================================
// ANDROID ACTIVITY LIFECYCLE'S FUNCTIONS:

//  @@@@@@@@@@@@
@Override 
  public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
}

//  @@@@@@@@@@@@
@Override 
  public void onStart() {
  super.onStart();
}

//  @@@@@@@@@@@@
void onPause(){
  super.onPause();
  if(snd!=null){
 
    snd.release();
    snd= null;
  }
}

//  @@@@@@@@@@@@
void onStop(){
  super.onStop();
  if(snd!=null){
 
    snd.release();
    snd= null;
  }
 
}
 
//  @@@@@@@@@@@@ 
void onDestroy(){
   super.onDestroy();
  if(snd!=null){
 
    snd.release();
    snd= null;
  }
}
