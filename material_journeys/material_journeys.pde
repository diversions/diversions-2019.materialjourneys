////////VR IMPORT////////////////
import processing.vr.*;

////////AUDIO IMPORT////////////////
import android.media.MediaPlayer;
import  android.content.res.Resources;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.Context;
import android.app.Activity;
import android.os.Bundle;

////////AUDIO SETUP////////////////
MediaPlayer snd = new MediaPlayer();
AssetFileDescriptor fd;
Context context;
Activity act;

////////ROOM SETUP////////////////
float roomX = 22000;
float roomY = 14000;
float roomZ = 5700; 

float roomBay = roomX/3;
float roomBeamW = 500;
float roomBeamH = 500;

int viewerOffset = -2900;
int viewerOffsetVal = -2900;

////////UI SETUP////////////////
int depth = 1000;

float tPosX = 0;
float tPosY = 0;
float pPosX = 0;
float pPosY = 0;
float rateChange = 0;

boolean trackTouch = false;

int backgroundColor = 0;
int buttonGrid = 11;
float buttonSize;
float iconSize;
float buttonPadding;
float buttonBlanks;
float buttonBufer;

////////COLLECTION CONTROL////////////////
int activeCollection = 0;
int newActiveCollection = 0;

////////AUDIO CONTROLS////////////////
boolean changeLanguage = false;
boolean french = true;
boolean playHeadsAudio = true;
boolean playHairAudio = true;
boolean playMiscAudio = true;
boolean playRestAudio = true;
boolean playBonesAudio = true;
boolean playOrganicAudio = true;
boolean playCoinsAudio = true;


////////COLLECTIONS////////////////
int collections = 7;

void setup() {
  
  fullScreen(MONO);    

////////AUDIO SETUP////////////////
  act = this.getActivity();
  context = act.getApplicationContext();

////////UI SETUP////////////////
  buttonSize = (width/buttonGrid);
  iconSize = (width/buttonGrid)/2;
  buttonPadding = buttonGrid-collections;
  buttonBlanks = (buttonSize * (buttonGrid - collections))/2;
  buttonBufer = 20;
  textMode(MODEL);
  textAlign(CENTER, CENTER);

////////INITIALIZE OBJECTS////////////////s
  headsList = new StringList();
  
  hObjs = new ArrayList<headObject>();
  haObjs = new ArrayList<hairObject>();
  bObjs = new ArrayList<boneObject>();
  oObjs = new ArrayList<organicObject>();
  cObjs = new ArrayList<coinObject>();
  rObjs = new ArrayList<restObject>();
  cbObjs = new ArrayList<collButtonObject>();
  lbObjs = new ArrayList<langButtonObject>();
  
  getCollections();
  
  initializeFly();
  
  initializeHeads();
  initializeHairs();
  initializeMisc();
  initializeBones();
  initializeOrganics();
  initializeRestraints();
  initializeCoins();
 
  initializeCollectionButtons();
  initializeLanguageButtons();

}

void draw() {
  
  //println(width, " + ", height);
  //println (mouseX, " + ", mouseY);
  
  background(backgroundColor);
  lights();
  
////////CHECK BUTTONS////////////////
  //for (int i = 0; i < touches.length; i++) {
    checkCollcetionButtons();
    checkLanguageButtons();
  //}  
  
////////DRAW UI////////////////
  drawUI();

  pushMatrix();
    translate(width/2,viewerOffset,height/2); 
////////DRAW ROOM////////////////
     drawRoom();
     
////////DRAW COLLECTIONS////////////////

      if (activeCollection==0){
        pushMatrix(); 
          //translate(0, -viewerOffset ,0);
          //translate(0, roomZ/2 ,-roomY/2);
          translate(0, viewerOffset*-2 ,-roomY/2);
          rotateX(radians(90)); 
          //translate(0,0,0); 
          drawCoins();
        popMatrix();
        if (playCoinsAudio == true){
          playCoins();
        }       
      }       
 
      if (activeCollection==1){
        pushMatrix();
          //translate(0, - viewerOffset,0); 
          //drawRest();
          drawRestraints();
        popMatrix();
        if (playRestAudio == true){
          playRest();
        }
      }
      
      if (activeCollection==2){
        pushMatrix();
          translate(0, - viewerOffset,0); 
          drawHairs();
        popMatrix();
        if (playHairAudio == true){
          playHair();
        }
      }
      
      if (activeCollection==3){
        pushMatrix();
          translate(0,-viewerOffset,0);
          rotateY(headsRotate);
          headsRotate = headsRotate + (PI/300);
          drawHeads();        
          if (playHeadsAudio == true){
            playHeads();
          }
        popMatrix();
      }
      
      if (activeCollection==4){
        drawMisc();
        if (playMiscAudio == true){
          playMisc();
        }
      }
      
      if (activeCollection==5){       
        pushMatrix();
          translate(0,viewerOffset + roomZ,0);
          drawOrganic();
          drawFly();
        popMatrix();
        if (playOrganicAudio == true){
          playOrganic();
        }   
      }     
      
      if (activeCollection==6){
          drawBones();
        if (playBonesAudio == true){
          playBones();
        }  
      }
       
            
  popMatrix();
  
}
