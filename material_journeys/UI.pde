void drawUI(){
  
  pushMatrix();
    eye(); 
    translate(-width/2,-height/2, 2000);
    
    for (int i = 0; i<collbuttonTotal; i++){
      cbObjs.get(i).drawCollButton();
    }
    
    for (int i = 0; i<languageButtons; i++){
      lbObjs.get(i).drawLangButton();
    }
  
  popMatrix();
}
