int organicDensity=200;
int organicWiggle = organicDensity/10;
int organicTotal = 0;
float organicSteps = roomZ/4;

organicObject oObj;
ArrayList<organicObject> oObjs;

class organicObject{
  
  int id;
  
  float x;
  float y;
  float z;  
  
  float hMax;
  
  float hTemp=0;
  float step = 0;
  

  organicObject(int index, int posX, int posY, float posZ){
    
    id = index;
    
    x = float(posX) - (roomX/2);
    y = float(posY) - (roomY/2);
    z = posZ/2;
    hMax = (roomZ/10) - abs(sqrt(sq(x)+sq(y)));
    //hMax = abs(sqrt(sq(x)+sq(y)))/2; 
    step = hMax/organicSteps;

  }
  
  void drawOrg(){
    
    strokeWeight(organicDensity/2);
    stroke(255,255,0,100);
  
    hTemp  = hTemp+step;

    if (hTemp<hMax){
      point(x,z-hTemp - random(-organicWiggle, organicWiggle),y);
    } else {
      point(x,z-hMax - random(-organicWiggle, organicWiggle),y);
    }

    //if (hTemp<hMax){
    //  float tRandA = random(-organicWiggle, organicWiggle);
    //  line (x-organicDensity/2, z-hTemp - tRandA, y-organicDensity/2, x+organicDensity/2, z-hTemp - tRandA, y-organicDensity/2); 
    //  line (x-organicDensity/2, z-hTemp - tRandA, y-organicDensity/2, x-organicDensity/2, z-hTemp - tRandA, y+organicDensity/2);
    //  line (x+organicDensity/2, z-hTemp - tRandA, y+organicDensity/2, x-organicDensity/2, z-hTemp - tRandA, y+organicDensity/2); 
    //  line (x+organicDensity/2, z-hTemp - tRandA, y+organicDensity/2, x+organicDensity/2, z-hTemp - tRandA, y-organicDensity/2); 
    //} else {
    //  float tRandB = random(-organicWiggle, organicWiggle);
    //  line (x-organicDensity/2, z-hMax - tRandB, y-organicDensity/2, x+organicDensity/2, z-hMax - tRandB, y-organicDensity/2); 
    //  line (x-organicDensity/2, z-hMax - tRandB, y-organicDensity/2, x-organicDensity/2, z-hMax - tRandB, y+organicDensity/2);
    //  line (x+organicDensity/2, z-hMax - tRandB, y+organicDensity/2, x-organicDensity/2, z-hMax - tRandB, y+organicDensity/2); 
    //  line (x+organicDensity/2, z-hMax - tRandB, y+organicDensity/2, x+organicDensity/2, z-hMax - tRandB, y-organicDensity/2); 
    //}
    
  }
    
}

void drawOrganic(){
  
  for (int i=0; i< organicTotal; i++){
    organicObject org = oObjs.get(i);
    org.drawOrg();
  }
      
}
void initializeOrganics(){
  
    for (int i = 0; i< roomX; i = i+organicDensity){
      for (int j = 0; j< roomY; j = j+organicDensity){
        oObj = new organicObject(organicTotal, i, j, roomZ);
        oObjs.add(oObj);
        organicTotal++;
      }
    }
  
}


void playOrganic(){
  try {

    fd = context.getAssets().openFd("organic_en.wav");
    if (french==true){
      fd = context.getAssets().openFd("organic_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
    }
    
    catch (IllegalArgumentException e) {
    e.printStackTrace();
    }
    catch (IllegalStateException e) {
    e.printStackTrace();
    } 
    catch (IOException e) {
    e.printStackTrace();
    }
    playOrganicAudio=false;
 
}
 
