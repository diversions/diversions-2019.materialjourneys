restObject rObj;
ArrayList<restObject> rObjs;

int restTotal = 0;
int restDensity = 500;
int restCount = 0;
int centreRestPos = viewerOffsetVal*2;

boolean resDown = false;

///////////////////////////////////////////////

class restObject{
  
  int id;
  int val = int(random(0,4000));
  
  float x;
  float y;
  float z;  
  
  
  int thickness = int(random(1,10));

  restObject(int index, int posX, int posY, float posZ){
    
    id = index;
    
    x = float(posX) - (roomX/2);
    y = float(posY) - (roomY/2);
    z = 0; //posZ-viewerOffsetVal;

  }
  
  void drawRest(){
    
   strokeWeight(thickness);
   stroke(0,255,255);
   noFill();
   
   float tempEndZ = z-centreRestPos;
   
   pushMatrix();
   //translate(0,-viewerOffsetVal,0);  
   if (val<restCount){
     if ((x==-roomX/2)||(x==roomX/2)){
       line (x,z,y,0,tempEndZ,0);
     }
     if ((y==-roomY/2)||(y==roomY/2)){
       line (x,z,y,0,tempEndZ,0);
     }
   }
   popMatrix();
     
  }

}
  
////////////////////////////////////////////////////////

void drawRestraints(){

  for (int i=0; i< restTotal; i++){
    rObjs.get(i).drawRest();
  }
 
  restCount++;
  
  if(centreRestPos<viewerOffsetVal*2){
    resDown=false;
  }  
  if(centreRestPos>viewerOffsetVal/2){
    resDown=true;
  } 

  
  if (resDown==false){
    centreRestPos+=10;
    viewerOffset+=9;
  }
  if (resDown==true){
    centreRestPos-=10;
    viewerOffset-=9;
  }
  
  println(centreRestPos);
  
////////////////  
  //centreRestPos++;
  //if(centreRestPos<0){
  //  centreRestPos=0;
  //}
  
  //viewerOffset++;
  //if(viewerOffset>1000){
  //  viewerOffset=1000;
  //}
 
}
 
void initializeRestraints(){
  
    println(centreRestPos);
  
    for (int i = 0; i<= roomX; i = i + restDensity){
      for (int j = 0; j<= roomY; j = j + restDensity){
        rObj = new restObject(restTotal, i, j, viewerOffsetVal);
        rObjs.add(rObj);
        restTotal++;
      }
    }
    //for (int i = 0; i<= roomX; i = i + restDensity){
    //  for (int j = 0; j<= roomY; j = j + restDensity){
    //    rObj = new restObject(restTotal, i, j, -roomZ/2);
    //    rObjs.add(rObj);
    //    restTotal++;
    //  }
    //}
}

void playRest(){
  try {
    
   fd = context.getAssets().openFd("rest_en.wav");
   if (french==true){
      fd = context.getAssets().openFd("rest_fr.wav");
    }
    snd.setDataSource(fd.getFileDescriptor(),fd.getStartOffset(), fd.getLength());
    snd.prepare();
    snd.start();
  }
  catch (IllegalArgumentException e) {
    e.printStackTrace();
  }
  catch (IllegalStateException e) {
    e.printStackTrace();
  } 
  catch (IOException e) {
    e.printStackTrace();
  }
  playRestAudio=false;
  
}
 
